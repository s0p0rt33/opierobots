# Opie robots website

This is a Hugo site. 

## Theme

Custom layout using (Bulma)[https://bulma.io] css framework.

## Preview 

Run it locally with 

    hugo -D server


## Deploy

Using gitlab deployment. Push commits to origin for auto publishing to gitlab pages.

    git push origin master

Wait a moment for the pipeline to process, then check.
